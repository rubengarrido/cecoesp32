#!/usr/bin/env bash

set -euo pipefail

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
BUILD_DIR="$DIR/../build"
rm -rf "$BUILD_DIR"
cd "$DIR/.."
. ./freertos/vendors/espressif/esp-idf/export.sh
cmake -S . -B "$DIR/../build" -DCMAKE_TOOLCHAIN_FILE=freertos/tools/cmake/toolchains/xtensa-esp32.cmake -GNinja