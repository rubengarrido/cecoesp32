# cecoESP32

Repository for testing ESP32 features and develop over aws IoT tools.

## Installation

*amazon-freertos* is needed as a submodule in order to have freertos and aws sdk. Once this reposiry is cloned, we need to init the submodule.

```bash
git submodule --init --recursive
```

## Usage
 In order to set up the toolchain and initialize the esp-idf tools, launch:
```python
cd scripts

# set up the toolchain and tools
./toolsIDF.sh

# configure the project to build 
./configure.sh

# First build
./build.sh
```